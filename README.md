# Étude fruitesque 🍌

## Infos 🍓

Cette étude fruitesque se base sur des méthodes **EXTREMEMENT SERIEUSE** et **IMPORTANTE** visant à trouver quel est objectivement le meilleur fruit.

Voici le formulaire mis en place pour interroger les différents participants: https://forms.gle/ts86XbYhnY7vG4Wc9

## Data 🥝

- ``fruit_consumption_frequency.csv`` : *Quelle est votre fréquence de consommation de ce fruit ?*
- ``fruit_taste.csv`` : *À quel point appréciez-vous le goût/la texture de ce fruit ?*
- ``fruit_practicality.csv`` : *À quel point ce fruit est-il pratique, que ce soit pour le transport, la variété de son utilisation ou encore sa consommation (difficile à éplucher/manger) ?*
- ``fruit_overall_impression.csv`` : *Si vous deviez mettre une note à ce fruit, ce serait quoi ? (Avis globale)*